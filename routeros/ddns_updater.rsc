# nsupdate.info DDNS Updater
# Based on https://gist.github.com/luispaulorsl/5df801cd8b4e4dff4af1519a2d40da10

:global publicIP;
:global abortUpdate;

:if ([:typeof $abortUpdate] != "bool") do={
    :set $abortUpdate false;
}

:if ($abortUpdate) do={
    :error "DDNS: Update aborted. Intervention required.";
}

:local currentIP;

:local ddnsPass "<password>";
:local ddnsHost "<hostname>";
:local ddnsURL "https://$ddnsHost:$ddnsPass@ipv4.nsupdate.info/nic/update";

:local response [/tool fetch url="https://ipv4.nsupdate.info/myip" as-value output=user];
:if ($response->"status" = "finished") do={
    :set currentIP ($response->"data");
} else={
    :error "DDNS: Unable to get current IP"
}

:if ($currentIP != $publicIP) do={
    :local response [/tool fetch url=$ddnsURL as-value output=user];
    :if ($response->"status" = "finished") do={
        :local data ($response->"data");
        :set $abortUpdate (!([:pick $data 0 4] = "good" || [:pick $data 0 5] = "nochg"));
        :set $publicIP $currentIP;

        :log info "DDNS: Update succeeded."
    } else={
        :log error "DDNS: Update failed.";
    }
} else={
    :log info "DDNS: No IP change.";
}
