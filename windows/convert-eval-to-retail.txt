# To get the valid Editions
DISM /Online /Get-TargetEditions

# Edition: Target edition (use above command to get the valid editons)
# ProductKey: Target <Edition> Key. Can be KMS Key
DISM /Online /Set-Edition:<Edition> /ProductKey:<ProductKey> /AcceptEula
