function Clear-Clipbloard {
    Add-Type -AssemblyName System.Windows.Forms
    [System.Windows.Forms.Clipboard]::Clear()
    $iter = 100
    while ($iter -gt 0) {
        $iter--
    
        $str = -join ((33..126) | Get-Random -Count 32 | % {[char]$_})
        Set-Clipboard $str
    }
    [System.Windows.Forms.Clipboard]::Clear()
}